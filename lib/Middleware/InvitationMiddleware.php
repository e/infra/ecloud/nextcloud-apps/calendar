<?php

namespace OCA\Calendar\Middleware;

use OCA\Calendar\BackgroundJob\SendInviteResponseMailJob;
use OCA\Calendar\Controller\InvitationMaybeController;
use OCA\Dav\Controller\InvitationResponseController;
use OCP\AppFramework\Http\Response;
use OCP\AppFramework\Middleware;
use OCP\BackgroundJob\IJobList;
use OCP\Calendar\IManager;
use OCP\Defaults;
use OCP\IConfig;
use OCP\IDBConnection;
use OCP\IL10N;
use OCP\ILogger;
use OCP\IRequest;
use OCP\IUserManager;
use OCP\L10N\IFactory;
use OCP\Mail\IMailer;

class InvitationMiddleware extends Middleware {
	private $request;
	private $jobList;
	/** @var IManager */
	private $calendarManager;
	private const translations = [
		"tentative" => [
			"meeting_title" => "Invitation Tentatively Accepted: %s",
			"meeting_body" =>
				"%s has tentatively accepted your invitation to %s on %s",
			"meeting_head" => "Tentatively Accepted",
		],
		"accept" => [
			"meeting_title" => "Invitation Accepted: %s",
			"meeting_body" => "%s has accepted your invitation to %s on %s",
			"meeting_head" => "Accepted",
		],
		"decline" => [
			"meeting_title" => "Invitation Declined: %s",
			"meeting_body" => "%s has declined your invitation to %s on %s",
			"meeting_head" => "Declined",
		],
	];

	public function __construct(
		IRequest $request,
		IConfig $config,
		IDBConnection $db,
		IManager $calendarManager,
		IMailer $mailer,
		IL10N $l10n,
		Defaults $defaults,
		ILogger $logger,
		IUserManager $iusermanager,
		IFactory $languageFactory,
		IJobList $jobList
	) {
		$this->request = $request;
		$this->config = $config;
		$this->db = $db;
		$this->calendarManager = $calendarManager;
		$this->mailer = $mailer;
		$this->l10n = $l10n;
		$this->defaults = $defaults;
		$this->logger = $logger;
		$this->iusermanager = $iusermanager;
		$this->languageFactory = $languageFactory;
		$this->jobList = $jobList;
	}

	public function afterController(
		$controller,
		$methodName,
		Response $response
	) {
		if (
			($controller instanceof InvitationMaybeController &&
				$methodName === "tentative") ||
			($controller instanceof InvitationResponseController &&
				($methodName === "accept" || $methodName === "decline") &&
				$response->getStatus() == 200 &&
				$response->getTemplateName() == "schedule-response-success")
		) {
			$token = $this->request->getParam("token");

			$this->jobList->add(SendInviteResponseMailJob::class, ['token' => $token,'methodName' => $methodName]);
		}


		return $response;
	}
}
