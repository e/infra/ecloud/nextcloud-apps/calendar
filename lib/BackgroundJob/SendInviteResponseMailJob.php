<?php

declare(strict_types=1);

/**
 * Calendar App
 *
 * @copyright 2021 Anna Larch <anna.larch@gmx.net>
 *
 * @author Anna Larch <anna.larch@gmx.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU AFFERO GENERAL PUBLIC LICENSE for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\Calendar\BackgroundJob;

use OCP\AppFramework\Utility\ITimeFactory;
use OCP\BackgroundJob\QueuedJob ;
use OCP\Calendar\IManager;
use OCP\Defaults;
use OCP\IDBConnection;
use OCP\IL10N;
use OCP\IUserManager;
use OCP\Mail\IMailer;
use OCP\Util;
use Psr\Log\LoggerInterface;
use Sabre\VObject\Parameter;
use Sabre\VObject\Reader;

class SendInviteResponseMailJob extends QueuedJob {
	private const translations = [
		"tentative" => [
			"meeting_title" => "Invitation Tentatively Accepted: %s",
			"meeting_body" =>
				"%s has tentatively accepted your invitation to %s on %s",
			"meeting_head" => "Tentatively Accepted",
		],
		"accept" => [
			"meeting_title" => "Invitation Accepted: %s",
			"meeting_body" => "%s has accepted your invitation to %s on %s",
			"meeting_head" => "Accepted",
		],
		"decline" => [
			"meeting_title" => "Invitation Declined: %s",
			"meeting_body" => "%s has declined your invitation to %s on %s",
			"meeting_head" => "Declined",
		],
	];


	public function __construct(ITimeFactory $time,
		IDBConnection $db,
		IManager $calendarManager,
		IMailer $mailer,
		IL10N $l10n,
		Defaults $defaults,
		IUserManager $iusermanager,

		LoggerInterface $logger) {
		parent::__construct($time);
		$this->db = $db;
		$this->logger = $logger;
		$this->calendarManager = $calendarManager;
		$this->mailer = $mailer;
		$this->l10n = $l10n;
		$this->defaults = $defaults;
		$this->iusermanager = $iusermanager;
	}

	private function getMailAttributes(
		string $methodName,
		array $translationData
	) {
		$meetingTitle = $this->l10n->t(
			self::translations[$methodName]["meeting_title"],
			$translationData["summary"]
		);
		$meetingBody = $this->l10n->t(
			self::translations[$methodName]["meeting_body"],
			[
				$translationData["attendee_name"],
				$translationData["summary"],
				$translationData["event_date"],
			]
		);
		$meetingHead = $this->l10n->t(
			self::translations[$methodName]["meeting_head"]
		);
		return [
			"meeting_title" => $meetingTitle,
			"meeting_body" => $meetingBody,
			"meeting_head" => $meetingHead,
		];
	}
	private function extract_emails_from($string) {
		$string = (string) $string;
		$matches = [];
		$found = preg_match("/mailto:(.*)/i", $string, $matches);
		if ($found === 1) {
			return $matches[1];
		}
		return "";
	}
	protected function run($arguments) {
		$token = $arguments['token'];
		$methodName = $arguments['methodName'];
		$defaultVal = '--';
		//$outdated = $this->service->sendInviteResponseMail($argument);
		$queryCalendarInvitations = $this->db->getQueryBuilder();
		$queryCalendarInvitations
				->select("id", "uid", "attendee", "organizer")
				->from("calendar_invitations")
				->where(
					$queryCalendarInvitations
						->expr()
						->eq(
							"token",
							$queryCalendarInvitations->createNamedParameter(
								$token
							)
						)
				);
		$stmt = $queryCalendarInvitations->execute();
		$row = $stmt->fetch(\PDO::FETCH_ASSOC);
		$uid = $row["uid"];
		$sender = $this->extract_emails_from($row["attendee"]);
		$recipient = $this->extract_emails_from($row["organizer"]);
		$userdata = $this->iusermanager->getByEmail($recipient);
		if (count($userdata) > 0) {
			$username = $userdata[0]->getUID();
			$principaluri = "principals/users/" . $username;
			$queryCalendarObjects = $this->db->getQueryBuilder();
			$queryCalendarObjects
					->select("co.id", "co.calendardata")
					->from("calendarobjects", "co")
					->innerJoin(
						"co",
						"calendars",
						"c",
						$queryCalendarObjects
							->expr()
							->eq("co.calendarid", "c.id")
					)
					->where(
						$queryCalendarObjects
							->expr()
							->eq(
								"c.principaluri",
								$queryCalendarObjects->createNamedParameter(
									$principaluri
								)
							)
					)
					->andWhere(
						$queryCalendarObjects
							->expr()
							->eq(
								"co.uid",
								$queryCalendarObjects->createNamedParameter(
									$uid
								)
							)
					);
			$stmt2 = $queryCalendarObjects->execute();

			if ($row2 = $stmt2->fetch(\PDO::FETCH_ASSOC)) {
				$vObject = Reader::read($row2["calendardata"]);
				$attendeeName = $sender;
				foreach ($vObject->VEVENT->ATTENDEE as $attendee1) {
					$attendee = $this->extract_emails_from($attendee1);
					if ($attendee == $sender) {
						if (!empty($attendee1["CN"])) {
							$attendeeName = $attendee1["CN"];
							break;
						}
					}
				}
				$organizerName = empty($vObject->VEVENT->ORGANIZER["CN"])
						? $recipient
						: $vObject->VEVENT->ORGANIZER["CN"];

				$SUMMARY = $vObject->VEVENT->SUMMARY;
				$datestart = (string) $vObject->VEVENT->DTSTART;
				if (str_contains($datestart, "T")) {
					$eventdate = date("F d, Y h:i A", strtotime($datestart));
				} else {
					$eventdate = date("F d, Y", strtotime($datestart));
				}
				$isAllDay = $vObject->VEVENT->DTSTART instanceof Property\ICalendar\Date;
				if (!$isAllDay) {
					$isFloating = $vObject->VEVENT->DTSTART->isFloating();
					if (!$isFloating) {
						$prop = $vObject->VEVENT->DTSTART->offsetGet('TZID');
						if ($prop instanceof Parameter) {
							$timezone = $vObject->VEVENT->DTSTART->offsetGet('TZID')->getValue();
							$eventdate = $eventdate.' ('.$timezone.')';
						}
					}
				}
				$translationData = [
					"summary" => $SUMMARY,
					"attendee_name" => $attendeeName,
					"event_date" => $eventdate,
				];
				$translations = $this->getMailAttributes(
					$methodName,
					$translationData
				);

				$data = [
					"attendee_name" => (string) $sender ?: $defaultVal,
					"invitee_name" => (string) $recipient ?: $defaultVal,
					"meeting_title" =>
						(string) $translations["meeting_title"] ?:
						$defaultVal,
				];
				$method = "reply";
				$emailTemplate = $this->mailer->createEMailTemplate(
					"dav.calendarInvite." . $method,
					$data
				);
				$emailTemplate->setSubject(
					$this->l10n->t($translations["meeting_title"], [
						$SUMMARY,
					])
				);
				$emailTemplate->addHeader();
				$emailTemplate->addHeading($translations["meeting_head"]);
				$emailTemplate->addBodyText(
					htmlspecialchars($translations["meeting_body"]),
					$translations["meeting_body"]
				);
				$emailTemplate->addFooter();
				try {
					$fromEMail = Util::getDefaultEmailAddress('invitations-noreply');
					$fromName = $this->l10n->t('%1$s via %2$s', [$attendeeName, $this->defaults->getName()]);
					$message = $this->mailer->createMessage();
					$message->setTo([$recipient => $organizerName]);
					$message->setFrom([$fromEMail => $fromName]);
					$message->setReplyTo([$sender => $attendeeName]);
					$message->useTemplate($emailTemplate);
					$this->mailer->send($message);
				} catch (\Exception $e) {
					// Log the exception and continue
					$this->logger->logException($e);
				}
			}
		}
	}
}
